export const environment = {
    production: true,
    host: 'https://millionspaces.com',
    bucket: 'https://res.cloudinary.com/dgcojyezg/image/upload/q_50/millionspaces',
    cldFolder: 'millionspaces',
    apiHost: 'https://api.millionspaces.com/api',
    apiHostNotification: 'https://notification.millionspaces.com',
    apiHostBase: 'https://api.millionspaces.com',
    envName: 'live'
};



