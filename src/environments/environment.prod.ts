export const environment = {
  production: true,
  bucket: 's3eventspace',
  host: 'https://stg.millionspaces.com',
  apiHost: 'https://tapi.millionspaces.com/api',
  apiHostNotification: 'tnotification.millionspaces.com',
  apiHostBase: 'https://tapi.millionspaces.com'
};
