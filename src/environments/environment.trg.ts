export const environment = {
    production: true,
    host: 'https://trg.millionspaces.com',
    bucket: 'https://res.cloudinary.com/dgcojyezg/image/upload/q_50/millionspaces',
    cldFolder: 'millionspaces',
    apiHost: 'https://tapi.millionspaces.com/api',
    apiHostNotification: 'https://tnotification.millionspaces.com',
    apiHostBase: 'https://tapi.millionspaces.com',
    envName: 'trg'
};
