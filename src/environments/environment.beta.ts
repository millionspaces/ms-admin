export const environment = {
    production: true,
    host: 'https://beta.millionspaces.com',
    bucket: 'https://res.cloudinary.com/dgcojyezg/image/upload/q_50/millionspaces',
    cldFolder: 'millionspaces',
    apiHost: 'https://bapi.millionspaces.com/api',
    apiHostNotification: 'https://bnotification.millionspaces.com',
    apiHostBase: 'https://bapi.millionspaces.com',
    envName: 'beta'
};
