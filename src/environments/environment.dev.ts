export const environment = {
    production: true,
    host: 'https://dev.millionspaces.com',
    bucket: 'https://res.cloudinary.com/dgcojyezg/image/upload/q_50/development',
    cldFolder: 'development',
    apiHost: 'https://dapi.millionspaces.com/api',
    apiHostNotification: 'https://dnotification.millionspaces.com',
    apiHostBase: 'https://dapi.millionspaces.com',
    envName: 'dev'
};