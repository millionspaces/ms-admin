export const environment = {
    production: true,
    host: 'https://qae.millionspaces.com',
    bucket: 'https://res.cloudinary.com/dgcojyezg/image/upload/q_50/millionspaces',
    cldFolder: 'millionspaces',
    apiHost: 'https://qapi.millionspaces.com/api',
    apiHostNotification: 'https://qnotification.millionspaces.com',
    apiHostBase: 'https://qapi.millionspaces.com',
    envName: 'qae'
};
