import {Component} from "@angular/core";
import {AuthService} from "../admin/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent{

  constructor(private auth: AuthService, private router: Router){

  }

  logOut(){
    this.auth.logOut().subscribe(response => {
      if(response === '100')
        this.router.navigate(['login']);
    })
  }
}
