import {Component} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'side-bar',
  templateUrl: './sidebar.component.html',
  styles: [`ul > li{ cursor: pointer; }`]
})
export class SidebarComponent{

  constructor(
    public router: Router,
    public route: ActivatedRoute
  ){

  }

  active: string = ''

  goToDashboard(){
    this.active = 'dashboard';
    this.router.navigate(['dashboard'], {relativeTo: this.route});
  }

  goToListSpace(){
    this.active = 'listspace';
    this.router.navigate(['spaces'], {relativeTo: this.route});
  }

  goToListHost(){
    this.active = 'listhost';
    this.router.navigate(['host-list'], {relativeTo: this.route});
  }

  goToGuestList(){
    this.active = 'listguest';
    this.router.navigate(['guest-list'], {relativeTo: this.route});
  }

  goToManageBookings(){
    this.active = 'managebookings';
    this.router.navigate(['manage-bookings'], {relativeTo: this.route});
  }

}
