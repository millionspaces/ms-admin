import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { PageNotFoundComponent } from "./page-not-found.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import {LoginComponent} from "./admin/login.comonent";
import {AdminLayoutComponent} from "./admin.layout.component";
import {SpaceListComponent} from "./spaces/space-list.component";
import {SpacesResolve} from "./spaces/shared/spaces.resolve";
import {GuestListComponent} from "./guest-list/guest-list.component";
import {HostListComponent} from "./host-list/host-list.component";
import {ManageBookingsComponent} from "./manage-bookings/manage-bookings.component";
import {SpaceEditComponent} from "./spaces/space-edit.component";
import {SpaceResolver} from "./spaces/shared/space-resolver.service";
import {EventTypeResolver} from "./spaces/shared/event-types-resolver.service";
import {AmenitiesResolver} from "./spaces/shared/amenities-resolver.service";
import {CancellationPolicyResolver} from "./spaces/shared/cancellation-policy.resolver.service";
import {AuthGuard} from "./shared/services/auth-guard.service";
import {AmenityUnitsResolver} from "./spaces/shared/amenity-units-resolver.service";

@NgModule({
  imports:[
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },
      {
        path: 'admin',
        component: AdminLayoutComponent,
        children:[
          {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
          {
            path : 'dashboard',
            component: DashboardComponent
          },
          {
            path : 'spaces',
            component: SpaceListComponent,
            resolve: { spaces: SpacesResolve },
          },
          {
            path : 'spaces/page/:pageIndex',
            component: SpaceListComponent,
            resolve: { spaces: SpacesResolve },
          },
          {
            path : 'spaces/:id/edit',
            component: SpaceEditComponent,
            resolve: {
              space: SpaceResolver,
              eventTypes: EventTypeResolver,
              amenities: AmenitiesResolver,
              amenityUnits: AmenityUnitsResolver,
              cancellationPolicies: CancellationPolicyResolver
            },
          },
          {
            path : 'host-list',
            component: HostListComponent
          },
          {
            path : 'guest-list',
            component: GuestListComponent
          },
          {
            path : 'manage-bookings',
            component: ManageBookingsComponent
          },
        ],
        canActivate: [ AuthGuard ]
      },
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: '**', component: PageNotFoundComponent },
    ],
    { useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule{}
