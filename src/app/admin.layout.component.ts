import {Component} from "@angular/core";

@Component({
  template:  `<side-bar></side-bar>
    
    <div class="main-panel">
      <navbar></navbar>
      <div class="content">
        <div class="container-fluid">
          <router-outlet></router-outlet>
        </div>
      </div>
      <footer-section></footer-section>
    </div>`
})
export class AdminLayoutComponent{

}
