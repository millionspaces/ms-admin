import {Component, Input, Output, EventEmitter} from "@angular/core";
import {AWSS3Service} from "./aws-s3.service";

@Component({
  selector: 'preview-image',
  styles: [`
        .space-image-box-thumbnail{
          max-height: 400px;
          max-width: 280px;
          background-size: cover;
          overflow: hidden;
        }
        .space-image-box-cover{
          max-height: 200px;
          max-width: 480px;
          background-size: cover;
          overflow: hidden;
          border: 1px solid rgba(0, 0, 0, 0.1);
        }
        .space-image-box-cover img{
          width: auto;
          height: auto;
          display: block;
          max-height: 200px;
          max-width: 480px;
        }
        .delete-photo {
          position: absolute;
          z-index: 1001;
          top: 10px;
          right: 10px;
          padding: 5px 9px;
          border: 1px solid rgba(0, 0, 0, 0.2);
          background-color: #fff;
          border-radius: 50%;
          width: 40px;
          height: 40px;
          color: #3e3b3b;
          display: none;
        }
        .delete-photo i:before {
          margin: 0;
          font-size: 20px;
        }
        .delete-photo:hover {
          background-color: #ccc;
        }
        .space-image-box-thumbnail:hover .delete-photo{
            display: block;
        }
        .space-image-box-cover:hover .delete-photo{
            display: block;
        }
        
    `],
  template: `
        <div class="col-xs-12 col-sm-8 m-b-10 p-x-0" 
        [class.space-image-box-thumbnail]="imageData?.window ==='thumbnail'" 
            [class.space-image-box-cover]="imageData?.window ==='cover'" 
                *ngIf="imageData?.response.Location">
        
            <img [src]="imageData?.response.Location" alt="your image">
            <a class="delete-photo" (click)="deleteImage()"><i class="fa fa-trash"></i></a>
        </div>
        <div class="loading" *ngIf="isLoading"></div>
    `
})
export class PreviewImagesDirectiveComponent{
  @Input() imageData;
  @Output() removed = new EventEmitter();
  isLoading = false;

  constructor(private awsService: AWSS3Service){
    console.log(this.imageData);
  }

  deleteImage(){
    this.isLoading = true;
    this.awsService.removeData({
      Key: this.imageData.response.Key,
    }).
    then(
      res => {
        this.imageData.response.Location = null;
        this.removed.emit({filName: this.imageData.response.Key, window: this.imageData.window})
        this.isLoading = false;
        console.log("DELETE SUCCESS");
      })
  }
}
