import {Injectable} from "@angular/core";
import {Subject, Observable} from "rxjs";

@Injectable()
export class ImageUploadService{

  private imageSource = new Subject<any>();

  getImageAsObservable(): Observable<any>{
    return this.imageSource.asObservable();
  }

  setImage(resp){
    this.imageSource.next(resp);
  }


}
