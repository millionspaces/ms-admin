import { NgModule } from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ModalComponent} from "./modal/modal.component";
import {PreviewImagesDirectiveComponent} from "./preview-images-directive.component";
import {ImageUploadModalComponent} from "./image-upload-modal.component";


@NgModule({
  imports:[
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ModalComponent,
    PreviewImagesDirectiveComponent,
    ImageUploadModalComponent
  ],
  exports: [
    ModalComponent,
    PreviewImagesDirectiveComponent,
    ImageUploadModalComponent
  ]
})
export class CoreModule{

}
