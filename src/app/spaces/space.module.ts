import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { SpaceListComponent } from "./space-list.component";
import {SpaceService} from "./shared/space.service";
import {SpacesResolve} from "./shared/spaces.resolve";
import {SpaceEditComponent} from "./space-edit.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SpaceResolver} from "./shared/space-resolver.service";
import {EventTypeResolver} from "./shared/event-types-resolver.service";
import {AmenitiesResolver} from "./shared/amenities-resolver.service";
import {CancellationPolicyResolver} from "./shared/cancellation-policy.resolver.service";
import {AmenityUnitsResolver} from "./shared/amenity-units-resolver.service";
import {CoreModule} from "../core/core.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    RouterModule.forChild([
      { path: 'spaces', component: SpaceListComponent, resolve: {'spaces': SpacesResolve}},
      {
        path: 'spaces/:id/edit',
        component: SpaceEditComponent,

      }
    ])
  ],
  declarations: [SpaceListComponent, SpaceEditComponent],
  providers: [
    SpaceService,
    SpacesResolve,
    SpaceResolver,
    AmenityUnitsResolver,
    EventTypeResolver,
    AmenitiesResolver,
    CancellationPolicyResolver
  ]
})
export class SpaceModule{

}
