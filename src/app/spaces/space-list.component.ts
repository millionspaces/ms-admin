import {Component, Input, OnInit, ChangeDetectionStrategy, OnChanges, AfterViewInit, OnDestroy} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {SpaceService} from "./shared/space.service";
import {ToasterService} from "../common/service/toastr.service";
import {ISpacePreview} from "./shared/space.model";
import {environment} from "../../environments/environment";

declare let $: any;

@Component({
  templateUrl: './space-list.component.html',
  styles: [`
    ul > li > a{cursor: pointer;}
    .fa-spin { font-size: 20px; }
    .fa-tv:before { font-size: 20px; cursor: pointer}
    .fa-edit:before { font-size: 20px; cursor: pointer}
  `],
})
export class SpaceListComponent implements OnInit, OnDestroy{

  @Input() spaces;
  pageIndex: number = 1;
  paramSub: any;
  spacesSub: any;
  env: any;

  constructor(
    private route: ActivatedRoute,
    private spaceService: SpaceService,
    private toastr: ToasterService,
    private router: Router
  ){}

  changeAcceptance(space: ISpacePreview): void{
      this.spaceService.approveOrDeclineSpace(space.id).subscribe(number => {
        console.log(number);
        if(number === 1){
          this.toastr.notifySuccess(`${space.name} has been activated`);
        }else if(number === 0){
          this.toastr.notifySuccess(`${space.name} has been deactivated`);
        }else{
          this.toastr.notifyError('Something went wrong!');
        }
      })
  }

  ngOnInit(){

      this.env = environment;

    this.spacesSub = this.route.data.subscribe(data => {
      if(data)
        this.spaces = data['spaces'];
        console.log('this.spaces',  this.spaces);
    })

    this.paramSub = this.route.params.subscribe(params => {
      if(params['pageIndex'])
        this.pageIndex = +params['pageIndex'];
    });

  }

  getPageSpaces(index: number): void{
    this.router.navigate(['admin/spaces/page', index])
  }

  goToSpaceProfile(space: any){
    window.open(`${this.env.host}/#/spaces/${space.id}/${this.replaceWhiteSpaces(space.name, '-')}`, '_blank');
  }

  ngOnDestroy(){
    this.paramSub.unsubscribe();
    this.spacesSub.unsubscribe();
  }

  replaceWhiteSpaces(text: string, replaceWith: string): string {
    return text.trim().replace(/\s/g, replaceWith);
  }


}
