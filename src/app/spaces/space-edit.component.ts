import {Component, OnInit, AfterViewInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {IEventType} from "./shared/event-type.model";
import {ISpace} from "./shared/space.model";
import {IAmentiy} from "./shared/amenity.model";
import {GmapService} from "../common/service/gmap.service";
import {ICancellationPolicy} from "./shared/cn-policy.model";
import {SpaceService} from "./shared/space.service";
import {Subscription} from "rxjs";
import {ImageUploadModalComponent} from "../core/image-upload-modal.component";
import {ImageUploadService} from "../core/image-upload.service";
import {environment} from "../../environments/environment";
import {ToasterService} from "../common/service/toastr.service";

declare let google: any;

@Component({
  templateUrl: './space-edit.component.html',
  styles: [`
    #map {
        height: 400px;
        width: 100%;
       }
       .btn-primary{
        width: 100%;
       }
  `]
})
export class SpaceEditComponent implements OnInit, AfterViewInit{

  bucket: string;
  isLoading = false;

  id: number;
  space: ISpace;
  eventTypes: IEventType[];
  amenities: IAmentiy[];
  amenityUnits = [];
  extraAmenities: IAmentiy[] = new Array<IAmentiy>();
  cancellationPolicies: ICancellationPolicy[];
  selectedPolicy: ICancellationPolicy;

  autoComplete: any;
  geoCodes: any;

  updatedEventTypes = [];
  updatedAmenities = [];
  updatedExtraAmenities: Array<any> = [];
  newExtraAmenities:Array<any> = [];

  spaceInformation: any = {
    thumbnailImgData: <any> null,
    coverImgData: <Array<any>> [],
  };

  // modal data
  modalComponentData = null;
  imageDataSubscription: Subscription;

  spaceCopy: any;


  constructor(
    private route: ActivatedRoute,
    private gmapService: GmapService,
    private spaceService: SpaceService,
    private uploadService: ImageUploadService,
    private toastr: ToasterService,
  ){
    this.bucket = environment.bucket;
  }

  ngOnInit(){

    this.space = this.route.snapshot.data['space'];
    this.eventTypes = this.route.snapshot.data['eventTypes'];
    this.amenities = this.route.snapshot.data['amenities'];
    this.amenityUnits = this.route.snapshot.data['amenityUnits'];
    this.cancellationPolicies = this.route.snapshot.data['cancellationPolicies'];

    this.spaceCopy = Object.assign({}, this.space);

    console.log('space', this.spaceCopy);

    this.imageDataSubscription = this.uploadService.getImageAsObservable().subscribe(
      imageData => {

        if (imageData.window === 'cover') {
          this.spaceInformation.coverImgData.push(imageData);
        }
        else if (imageData.window === 'thumbnail') {
          this.spaceInformation.thumbnailImgData = imageData;
        }
      });


    let thumImage = {
      response: {
        Key: this.space.thumbnailImage,
        Location: "https://s3-ap-southeast-1.amazonaws.com/"+this.bucket+"/"+this.space.thumbnailImage
      },
      window: 'thumbnail'
    }

    this.spaceInformation.thumbnailImgData = thumImage;

    this.space.images.forEach((cImage, idx) => {
      let coverImage = {
        response: {
          Key: this.space.images[idx],
          Location: "https://s3-ap-southeast-1.amazonaws.com/"+this.bucket+"/"+this.space.images[idx]
        },
        window: 'cover'
      }
      this.spaceInformation.coverImgData.push(coverImage);
    })

    //create new array with 'selected' property - used for template
    this.updatedEventTypes = this.eventTypes.map(eventType => {
       eventType.selected = eventType.selected || false;
       return eventType;
    });

    //update array for the selected events in the space
    this.updatedEventTypes.forEach(eventType => {
      let keepGoing = true;
      this.spaceCopy.eventType.forEach(eventId => {
        if(keepGoing){
          if(eventType.id === eventId) {
            eventType.selected = true;
            keepGoing = false;
          }
        }
      })
    })

    //create new array with 'selected' property - used for template
    this.updatedAmenities = this.amenities.map(amenity => {
      amenity.selected = amenity.selected || false;
      return amenity;
    });

    //update array for the selected amenities in the space
    this.updatedAmenities.forEach(amenity => {
      let keepGoing = true;
      this.spaceCopy.amenity.forEach(amenityId => {
        if(keepGoing){
          if(amenity.id === amenityId) {
            amenity.selected = true;
            keepGoing = false;
          }
        }
      })
    })


    this.updatedExtraAmenities = this.space.extraAmenity.slice();


    this.updatedExtraAmenities.forEach(exAmenity => {
      let amenityUnit = this.findAmenityUnit(exAmenity.amenityUnit)
      exAmenity.amenityUnitName = amenityUnit.name || '';
    })

    console.log('updatedExtraAmenities', this.updatedExtraAmenities);

    this.selectedPolicy = this.cancellationPolicies.find(policy => policy.id === this.space.cancellationPolicy);



    /*this.space.extraAmenity.forEach(exAmenity => {
        let amenity = this.amenities.find(amenity => amenity.id === exAmenity.amenityId)
        amenity.rate = exAmenity.extraRate;
        this.extraAmenities.push(amenity);
        console.log(this.extraAmenities);
      }
    )*/
  }
  addExtraAmenity(){
    this.newExtraAmenities.push({
      amenityId: null,
      amenityUnit: null,
      amenityUnitName: 'Select',
      extraRate: 0,
      name: 'Select',
    });
  }

  removeUpdatedExtraAmenity(index: number){
    //let selectedAmenity = this.updatedExtraAmenities.find(amenity => amenity.amenityId === id);
    this.updatedExtraAmenities.splice(index, 1);
  }

  removeNewExtraAmenity(index: number){
    this.newExtraAmenities.splice(index, 1)
  }

  selectAmenity(newAmenity, amenity){
    newAmenity.amenityId = amenity.id;
    newAmenity.name = amenity.name;
  }

  setAmenityUnit(amenity, unit){
    amenity.amenityUnit = unit.id;
    amenity.amenityUnitName = unit.name;
  }

  findAmenityUnit(id: number){
    return this.amenityUnits.find(unit => unit.id === id)
  }

  onChangeEvent(event){
    event.selected = !event.selected;
    console.log('updatedEventTypes', this.updatedEventTypes);
  }

  onChangeAmenity(amenity){
    amenity.selected = !amenity.selected;
    console.log('updatedAmenities', this.updatedAmenities);
  }

  saveSpace(formValues){
    console.log(formValues);
    console.log(this.eventTypes);


    /*let object = {
      name: formValues.title,
      addressLine1: formValues.address,
      description: formValues.description,
      size: formValues.size,
      participantCount: formValues.participantCount,
      ratePerHour: formValues.ratePerHour,
      user: 1,
      cancellationPolicy: formValues.selectedPolicy.id,
      latitude:this.geoCodes.lat,
      longitude:this.geoCodes.lng,
      eventType:[1],
      amenity: [1,2,3],
      extraAmenity: [ {"amenityId": 2, "extraRate": 50},{ "amenityId": 1,"extraRate": 50},{ "amenityId": 3,"extraRate": 50} ],
    }*/

  }


  ngAfterViewInit(){
    /*var location = {lat: +this.space.latitude, lng: +this.space.longitude};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: location
    });
    var marker = new google.maps.Marker({
      position: location,
      map: map
    });*/

    var mapOptions = {
      zoom: 15,
      center: {lat: +this.spaceCopy.latitude, lng: +this.spaceCopy.longitude},
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };

    this.gmapService.setMap(mapOptions, 'map');

    let input: HTMLInputElement = (<HTMLInputElement>document.getElementById("search-box"));
    this.initAutoComplete(input);

  }

  private initAutoComplete(input: HTMLInputElement): void {
    this.autoComplete = new google.maps.places.Autocomplete(input, { componentRestrictions: {country: "lk"} });
    google && google.maps && google.maps.event && google.maps.event.addListener(this.autoComplete, 'place_changed', () => {

      this.spaceCopy.addressLine1 =  input.value;
      this.gmapService.findGeoCode(input.value).subscribe(
        geoCode => {
          this.spaceCopy.latitude = geoCode.lat;
          this.spaceCopy.longitude = geoCode.lng;

          this.gmapService.setSelectedGeoCodes({
            lat: geoCode.lat,
            lng: geoCode.lng
          });
          this.gmapService.targetLocation(this.autoComplete.getPlace().geometry.location);
        },

        error => { console.log(error)}
      );





    });
  }


  addMoreExtras(){
    let amenity: IAmentiy = {
      name: 'Select',
      rate: 0
    }
    this.extraAmenities.push(amenity);
  }

  removeExtra(amenityId){
    console.log(amenityId);
    this.extraAmenities = this.extraAmenities.filter(amenity => amenity.id !== amenityId);
  }

  selectPhotos(window){
    this.createImageUploadModal(window);
  }

  createImageUploadModal(window) {
    this.modalComponentData = {
      component: ImageUploadModalComponent,
      inputs: {
        window : window
      }
    }
  }

  imageRemoved($event){

    if($event.window === 'cover'){
      let index = 0;
      this.spaceInformation.coverImgData.forEach(imgData => {
        if($event.filName === imgData.response.Key){
          this.spaceInformation.coverImgData.splice(index, 1);
          return;
        }
        index++;
      });
    }
    else if($event.window === 'thumbnail'){
      if($event.filName === this.spaceInformation.thumbnailImgData.response.Key){
        this.spaceInformation.thumbnailImgData = null;
      }
    }
  }

  updateSpace(){
    this.isLoading = true;
    //console.log('update space', this.spaceCopy);
    try {

      //set eventTypes and amenities
      let selectedEventTypes = this.updatedEventTypes.filter(eventType => eventType.selected === true).map(eventType => eventType.id);
      let selectedAmenities = this.updatedAmenities.filter(amenity => amenity.selected === true).map(amenity => amenity.id);

      this.spaceCopy.eventType = selectedEventTypes;
      this.spaceCopy.amenity = selectedAmenities;

     /* //set cover photos and thumbnails
      let coverImages = this.spaceInformation.coverImgData.map(imageData => imageData.response.Key);
      let thumbnailImage = this.spaceInformation.thumbnailImgData.response.Key;

      this.spaceCopy.thumbnailImage = thumbnailImage;
      this.spaceCopy.images = coverImages;*/

    }catch (e){
      console.log(e);
      this.isLoading = false;
      this.toastr.notifyError('Error');
    }

    this.spaceService.updateSpace(this.spaceCopy).subscribe(resp => {
      this.toastr.notifySuccess(`${this.spaceCopy.name} ${resp.status}`);
      this.isLoading = false;
    })


  }

}
