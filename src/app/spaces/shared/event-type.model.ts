export interface IEventType{
  id: number;
  name: string;
  icon: string;
  selected?: boolean;
}
