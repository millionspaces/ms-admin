
export interface ISpacePreview{
  id: number;
  name: string;
  address: string;
  hostName: string;
  rating: number;
  approved: number;
}

export interface ISpace{
  id: number;
  name: string;
  location: string;
  addressLine1: string;
  addressLine2: string;
  description: string;
  size: number;
  participantCount: number;
  ratePerHour: number;
  user: number;
  cancellationPolicy: number;
  images: string[];
  eventType: number[];
  amenity: number[];
  extraAmenity: any[];
  latitude: string;
  longitude: string;
  thumbnailImage: string;
}



