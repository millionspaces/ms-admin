import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { SpaceService } from "./space.service";
import { ISpace } from "./space.model";

@Injectable()
export class SpaceResolver implements Resolve<ISpace>{

  constructor(
    private router: Router,
    private spaceService: SpaceService
  ){}

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<ISpace>{
    let id = route.params['id'];

    if(isNaN(id)){
      console.log(`Space id was not a number: ${id}`);
      this.router.navigate(['/spaces']);
      return Observable.of(null);
    }

    return this.spaceService.getSpace(+id)
      .map(space => {
        if(space)
          return space;
        console.log(`Space was not found: ${id}`);
        this.router.navigate(['/spaces']);
        return null;
      })
      .catch(error => {
        console.log(`Retrieval Error: ${error}`);
        this.router.navigate(['/spaces']);
        return Observable.of(null);
      });

  }
}
