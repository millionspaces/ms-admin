export interface ICancellationPolicy{
  id: number;
  name: string;
  description: string;
  selected?: boolean;
}
