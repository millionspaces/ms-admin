import {Resolve} from "@angular/router";
import {Injectable} from "@angular/core";
import {SpaceService} from "./space.service";
import {Observable} from "rxjs";

@Injectable()
export class AmenityUnitsResolver implements Resolve<any>{

  constructor(private spaceService: SpaceService){}

  resolve(): Observable<any>{

    return this.spaceService.getAmenityUnits();

  }
}
