
import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot} from "@angular/router";
import {SpaceService} from "./space.service";
import {Observable} from "rxjs";
import {ISpacePreview} from "./space.model";
@Injectable()

export class SpacesResolve implements Resolve<any>{

  constructor(private spaceService: SpaceService){}

  resolve(route: ActivatedRouteSnapshot): Observable<ISpacePreview[]>{
    let pageIndex = route.params['pageIndex'];

    if(pageIndex)
      return this.spaceService.getSpacesList(+route.params['pageIndex'] - 1);

    return this.spaceService.getSpacesList(0);

  }

}
