import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {IEventType} from "./event-type.model";
import {Observable} from "rxjs/Observable";
import {SpaceService} from "./space.service";

@Injectable()
export class EventTypeResolver implements Resolve<IEventType[]>{

    constructor(
      private spaceService: SpaceService,
      private router: Router
    ){}

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<IEventType[]>{

      return this.spaceService.getEventTypeList()
        .map(eventTypes => {
          if(eventTypes)
            return eventTypes;
          console.log('Not found');
          this.router.navigate(['/spaces']);
          return null;
        })
        .catch(error => {
          console.log(`Retrieval Error: ${error}`);
          this.router.navigate(['/spaces']);
          return Observable.of(null);
        });
    }

}
