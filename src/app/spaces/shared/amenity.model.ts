export interface IAmentiy{
  id?: number;
  name: string;
  icon?: string;
  amenityUnit?: number;
  amenityUnitsDto?: any;
  selected?: boolean;
  rate?: number;

}
