import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {ICancellationPolicy} from "./cn-policy.model";
import {SpaceService} from "./space.service";

@Injectable()
export class CancellationPolicyResolver implements Resolve<ICancellationPolicy[]>{

  constructor(private spaceService: SpaceService){}

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<ICancellationPolicy[]>{

    return this.spaceService.getCancellationPolicies()
      .map(cnPolicies => {
        if(cnPolicies)
          return cnPolicies

        console.log('Policies Not found');
        return null;

      })
      .catch(error => {
        console.log(`Retrieval Error: ${error}`);
        return Observable.of(null);
      });

  }


}
