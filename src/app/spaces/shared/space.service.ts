import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Http, Response } from "@angular/http";
import { ISpacePreview, ISpace } from "./space.model";
import { ServerConfigService } from "../../server.config.service";
import { environment } from '../../../environments/environment';
import { IEventType } from "./event-type.model";
import { IAmentiy } from "./amenity.model";
import { ICancellationPolicy } from "./cn-policy.model";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

@Injectable()
export class SpaceService{

  baseUrlAdmin: string;
  baseUrl: string;
  eventTypesUrl: string;
  amenitiesUrl: string;
  cnPolicyUrl: string;
  amenityUnitsUrl: string;

  constructor(
    private http: Http,
    private serverConfig: ServerConfigService
  ){
    this.baseUrlAdmin = environment.apiHost+'/admin/space';
    this.baseUrl = environment.apiHost+'/space';
    this.eventTypesUrl = environment.apiHost+'/common/eventTypes';
    this.amenitiesUrl = environment.apiHost+'/common/amenities';
    this.amenityUnitsUrl = environment.apiHost+'/common/amenityUnits';
    this.cnPolicyUrl = environment.apiHost+'/common/cancellationPolicies';
  }

  getSpace(spaceId: number): Observable<ISpace>{
    return this.http
      .get(`${this.baseUrl}/${spaceId}`, this.serverConfig.getCookieHeader())
      .map(this.extractData)
      .catch(this.handleError);
  }

  updateSpace(space): Observable<any>{
    return this.http
      .put(this.baseUrl+'/'+space.id, JSON.stringify(space), this.serverConfig.getJsonHeader())
      .map(this.extractData)
      .catch(this.handleError);
  }

  getSpacesList(pageIndex: number): Observable<ISpacePreview[]> {
    return this.http
      .get(`${this.baseUrlAdmin}/${pageIndex}`, this.serverConfig.getCookieHeader())
      .map((response: Response) => <ISpacePreview[]>response.json())
      //.do(spaces => console.log(spaces))
      .catch(this.handleError);
  }

  getEventTypeList(): Observable<IEventType>{
    return this.http
      .get(this.eventTypesUrl, this.serverConfig.getCookieHeader())
      .map(this.extractData)
      .do(eventTypes => console.log('EventTypes', eventTypes))
      .catch(this.handleError);
  }

  getAmenityList(): Observable<IAmentiy>{
    return this.http
      .get(this.amenitiesUrl, this.serverConfig.getCookieHeader())
      .map(this.extractData)
      .do(amenities => console.log('Amenities', amenities))
      .catch(this.handleError);
  }

  getAmenityUnits(): Observable<any>{
    return this.http
      .get(this.amenityUnitsUrl, this.serverConfig.getCookieHeader())
      .map(this.extractData)
      .catch(this.handleError);
  }

  approveOrDeclineSpace(spaceId: number): Observable<number>{
      let obj = {id: spaceId};
      return this.http
        .post(`${this.baseUrlAdmin}/acceptance`, JSON.stringify(obj), this.serverConfig.getJsonHeader())
        .map(this.extractData)
        .catch(this.handleError);
  }

  getCancellationPolicies(): Observable<ICancellationPolicy[]>{
    return this.http
      .get(this.cnPolicyUrl, this.serverConfig.getCookieHeader())
      .map(this.extractData)
      .do(policies => console.log(policies))
      .catch(this.handleError);
  }

  private extractData(response: Response) {
    return response.json();
  }

  private handleError(error: any): Observable<any> {
    return Observable.throw(error.json().error || 'Server Error');
  }

}
