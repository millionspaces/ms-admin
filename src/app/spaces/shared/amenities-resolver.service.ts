import {Injectable} from "@angular/core";
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {SpaceService} from "./space.service";
import {IAmentiy} from "./amenity.model";

@Injectable()
export class AmenitiesResolver implements Resolve<IAmentiy[]>{

  constructor(
    private spaceService: SpaceService,
    private router: Router
  ){}

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<IAmentiy[]>{

    return this.spaceService.getAmenityList()
      .map(amenities => {
        if(amenities)
          return amenities;
        console.log('Not found');
        this.router.navigate(['/spaces']);
        return null;
      })
      .catch(error => {
        console.log(`Retrieval Error: ${error}`);
        this.router.navigate(['/spaces']);
        return Observable.of(null);
      });
  }

}
