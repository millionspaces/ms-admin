import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from "@angular/http";

@Injectable()
export class ServerConfigService{

  private jsonHeader: RequestOptions;
  private loginHeader: RequestOptions;
  private cookieHeader: RequestOptions;
  private corsHeader: RequestOptions;

  private _geoCodePath: string = "https://maps.googleapis.com/maps/api/geocode";

  constructor() {
    this.initRequestHeaders();
  }

  initRequestHeaders(){
    let headersJson = new Headers();
    headersJson.append("Content-Type", "application/json");

    let headersLogin = new Headers();
    headersLogin.append("Content-Type", "application/x-www-form-urlencoded");

    let headersCORS = new Headers();
    headersCORS.append("Access-Control-Allow-Origin", "*");
    headersCORS.append("Access-Control-Allow-Methods", "DELETE, HEAD, GET, OPTIONS, POST, PUT");
    headersCORS.append("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    headersCORS.append("Access-Control-Allow-Headers", "Content-Type");

    this.jsonHeader = new RequestOptions({ headers: headersJson, withCredentials: true });
    this.loginHeader = new RequestOptions({ headers: headersLogin, withCredentials: true });
    this.corsHeader = new RequestOptions({ headers : headersCORS });
    this.cookieHeader = new RequestOptions({ withCredentials: true });
  }

  getJsonHeader(){
    return this.jsonHeader;
  }

  getLoginHeader(){
    return this.loginHeader;
  }

  getCookieHeader(){
    return this.cookieHeader;
  }

  getGeoCodeJsonPath(){
    return this._geoCodePath+"/json?";
  }


}

