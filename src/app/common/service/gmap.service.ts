import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Http, Response} from "@angular/http";
import {ServerConfigService} from "../../server.config.service";

declare let google: any;

@Injectable()
export class GmapService{

  private _geoCodeApiKey = "AIzaSyBu8boLLCd-7ea_aptaWUBd4b-GahPwPSo";
  marker: any;
  mapOnSpaceEdit;
  selectedGeoCodes : any = null;

  constructor(private http: Http, private serverConfig: ServerConfigService){}

  findGeoCode(address: string): Observable<any>{
    return this.http
      .get(`${this.serverConfig.getGeoCodeJsonPath()}address=${address}&key${this._geoCodeApiKey}`)
      .map((response: Response) => response.json().results)
      .map(results => {
        return {
          lat : results[0].geometry.location.lat,
          lng : results[0].geometry.location.lng
        }
      });
  }

  setSelectedGeoCodes(geoCodes){
    this.selectedGeoCodes = geoCodes;
  }

  targetLocation(geocode: any) {

    if(this.marker)
      this.marker.setMap(null);

    this.marker = new google.maps.Marker({
      position: geocode,
      draggable: true,
      animation: google.maps.Animation.DROP,
      icon: 'https://stg.millionspaces.com/assets/img/map/32-32-normal.png'
    });
    /*google.maps.event.addListener(this.marker, 'dragend', function (marker) {
      console.log(marker.latLng.lat());
      console.log(marker.latLng.lng());
      this.setSelectedGeoCodes({
        lat: marker.latLng.lat(),
        lng: marker.latLng.lng(),
      })
    }.bind(this));*/
    this.marker.setMap(this.mapOnSpaceEdit);
    this.mapOnSpaceEdit.panTo(geocode);
  }

  setMap(mapOptions, id){

    if(this.marker)
      this.marker.setMap(null);

    this.mapOnSpaceEdit = new google.maps.Map(document.getElementById(id), mapOptions);
    this.setMarker(mapOptions);
    /*setTimeout(function() {
      google.maps.event.trigger(this.mapOnSpaceCreate, "resize");
    }, 100);*/
  }

  setMarker(mapOptions){


    this.marker = new google.maps.Marker({
      position: new google.maps.LatLng(mapOptions.center.lat, mapOptions.center.lng),
      map: this.mapOnSpaceEdit,
      draggable: true,
      icon: 'https://stg.millionspaces.com/assets/img/map/32-32-normal.png'
    });
    /*google.maps.event.addListener(this.marker, 'dragend', function (marker) {
      console.log(marker.latLng.lat());
      console.log(marker.latLng.lng());
      this.setSelectedGeoCodes({
        lat: marker.latLng.lat(),
        lng: marker.latLng.lng(),
      })
    }.bind(this));*/
  }


}
