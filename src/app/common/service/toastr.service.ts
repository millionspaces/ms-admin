import {Injectable} from "@angular/core";

declare let $;

@Injectable()
export class ToasterService{

  toastrTime: 2000;

  notifySuccess(message: string){
    $.notify(
      { icon: 'fa fa-check', message: message },
      { type: 'success', timer: this.toastrTime }
    );
  }
  notifyError(message: string){
    $.notify(
      { icon: 'fa fa-times', message: message },
      { type: 'danger', timer: this.toastrTime }
    );
  }
  notifyWarning(message: string){
    $.notify(
      { icon: 'fa fa-exclamation-triangle', message: message },
      { type: 'warning', timer: this.toastrTime }
    );
  }
  notifyInfo(message: string){
    $.notify(
      { icon: 'fa fa-info-circle', message: message },
      { type: 'info', timer: this.toastrTime }
    );
  }
}
