import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {AuthService} from "../../admin/auth.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private auth: AuthService, private router: Router){

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
      return this.checkLoggedIn();
    }

    checkLoggedIn(): Observable<any> {
      return this.auth.getLoggedUser().map(user => {
        if(user)
          return true;
      }).catch(() => {
        this.router.navigate(['/login']);
        return Observable.of(false);
      });
    }


}
