import {Injectable} from "@angular/core";

declare let CryptoJS: any;

@Injectable()
export class CryptoService{

  constructor(){}

  key: string = "t2s67p1a345o7rzm";

  md5Hex(message){
    return this.toHex(CryptoJS.MD5, message);
  }

  sha1Hex(message){
    return this.toHex(CryptoJS.SHA1, message);
  }

  sha256Hex(message){
    return this.toHex(CryptoJS.SHA256, message);
  }

  sha224Hex(message){
    return this.toHex(CryptoJS.SHA224, message);
  }

  sha512Hex(message){
    return this.toHex(CryptoJS.SHA512, message);
  }

  sha384Hex(message){
    return this.toHex(CryptoJS.SHA384, message);
  }

  sha3Hex(message){
    return this.toHex(CryptoJS.SHA3, message);
  }

  toHex(hasher, message){
    return hasher(message).toString(CryptoJS.enc.Hex);
  }

  encryptData(data){
    let key = CryptoJS.enc.Latin1.parse('t2s67p1a345o7rzm');
    let iv = CryptoJS.enc.Latin1.parse('abcdefghabcdefgd');
    let encrypted = CryptoJS.AES.encrypt(
      JSON.stringify(data),
      key,
      { iv:iv, mode:CryptoJS.mode.CBC, padding:CryptoJS.pad.ZeroPadding }
    );
    return encrypted.toString();
  }



}
