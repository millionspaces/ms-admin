import {Injectable} from "@angular/core";
import {Subject} from "rxjs/Subject";
import {IAdmin} from "./admin.model";

@Injectable()
export class AdminService{

  private adminUser: IAdmin;
 // private userSubject = new Subject<IAdmin>();

  setAdminUser(user: IAdmin){
    this.adminUser = user;
    //this.userSubject.next(user);
  }

  getAdminUser(): IAdmin{
    return this.adminUser;
  }

  /*getAdminUserObservble(){
    return this.userSubject.asObservable();
  }

  clearUser(){
    this.userSubject.next();
  }*/

  isAuthenticated(){
    return this.getAdminUser() ? true : false;
  }

}
