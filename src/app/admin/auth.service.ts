import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Http } from "@angular/http";
import { environment } from "../../environments/environment";

import { ServerConfigService } from "../server.config.service";
import { CryptoService } from "../shared/services/crypto.service";
import { IAdmin } from "./admin.model";
import { AdminService } from "./admin.service";

import 'rxjs/operator/map';
import 'rxjs/operator/catch';

@Injectable()
export class AuthService {

  private baseUrl;
  private baseUrlUserGrant: string;
  private baseUrlDeviceLogout: string;

  constructor(
    private http: Http,
    private serverConfig: ServerConfigService,
    private crypto: CryptoService,
    private adminService: AdminService
  )
  {
    this.baseUrl = `${environment.apiHost}/user`;
    this.baseUrlUserGrant = `${environment.apiHost}/grant_eventspace_security`;
    this.baseUrlDeviceLogout = `${environment.apiHost}/device_logout`;
  }

  login(user: IAdmin): Observable<IAdmin>{
    return this.http
      .post(
        this.baseUrlUserGrant,
        'username='+user.email+'&password='+this.crypto.sha256Hex(user.password),
        this.serverConfig.getLoginHeader())
      .map(response => {
        let user: IAdmin = response.json();
        localStorage.setItem('U_ADMIN', 'Y');
        this.adminService.setAdminUser(user);
        return user;
      })
      .catch(this.handleError);
  }

  logOut(): Observable<any>{
    return this.http
      .get(
        this.baseUrlDeviceLogout,
        this.serverConfig.getCookieHeader())
      .map( response => {
        if(response){
          localStorage.removeItem('U_ADMIN');
          this.adminService.setAdminUser(null);
          return response.text();
        }
        return null;
      })
      .catch(this.handleError);
  }

  getLoggedUser(): Observable<IAdmin>{
    if(!localStorage.getItem('U_ADMIN'))
      return null;

    return this.http
      .get(
        this.baseUrl,
        this.serverConfig.getLoginHeader())
      .map(response => {
        let user: IAdmin = response.json();
        return user;
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Observable<any> {
    return Observable.throw(error.json().error || 'Server Error');
  }
}
