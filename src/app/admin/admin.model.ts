export interface IAdmin {
  id?: number;
  email: string;
  password?: string;
  name?: string;
  mobileNumber?: string;
  isTrustedUser?: boolean;
}
