import {Component} from "@angular/core";
import {AuthService} from "./auth.service";
import {Router} from "@angular/router";

@Component({
  template: `
    <div class="middle-box">
      <div class="padding-bottom-60" align="center">
        <img src="assets/img/mainlogo.png">
      </div>
      <form #loginForm="ngForm" (ngSubmit)="login(loginForm.value)" autocomplete="off" novalidate>
        <div class="form-group">
          <input ngModel name="email" id="email" type="text" class="form-control" placeholder="User Name or Email" required />
        </div>
        <div class="form-group">
          <input ngModel name="password" id="password" type="password" class="form-control" placeholder="Password" required />
        </div>
        <button type="submit" class="btn btn-primary btn-block">Login</button>
        <div class="caption-tag">
          <span>All rights reserved © MillionSpaces.com  </span>
        </div>
      </form>
      
    </div>
  `,
  styles: [`
    .middle-box {
      max-width: 450px;
      margin: auto;
      margin-top: 5%;
      background-color: #f7f7f7;
      padding: 70px;
      min-height: 420px;
      border: 5px solid #61a0b9;
    }
    .caption-tag {
      text-align: center;
      color: #61a0b9;
      margin-top: 50px;
    }
    .lockscreen.middle-box {
      width: 200px;
      padding-top: 110px;
    }
    .loginscreen.middle-box {
      width: 300px;
    }
    .loginColumns {
      max-width: 800px;
      margin: 0 auto;
      padding: 100px 20px 20px 20px;
    }
    .passwordBox {
      max-width: 460px;
      margin: 0 auto;
      padding: 100px 20px 20px 20px;
    }
    .logo-name {
      color: #e6e6e6;
      font-size: 180px;
      font-weight: 800;
      letter-spacing: -10px;
      margin-bottom: 0;
    }
    .middle-box h1 {
      font-size: 170px;
    }
    .wrapper .middle-box {
      margin-top: 140px;
    }
  `]
})
export class LoginComponent{

  user: any;

  constructor(
    private auth: AuthService,
    private router: Router
  ){

  }

  login(formValues){
    this.user = { email: formValues.email, password: formValues.password }

    this.auth.login(this.user).subscribe(user => {
      console.log('Admin user', user);
      if(user)
        this.router.navigate(['admin']);
    });
  }
}
