import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {ServerConfigService} from "./server.config.service";
import {ToasterService} from "./common/service/toastr.service";
import {AppRoutingModule} from "./app-routing.module";
import {SpaceModule} from "./spaces/space.module";
import {PageNotFoundComponent} from "./page-not-found.component";
import {NavbarModule} from "./navbar/navbar.module";
import {SidebarModule} from "./sidebar/sidebar.module";
import {FooterComponent} from "./footer/footer.component";
import {GmapService} from "./common/service/gmap.service";
import {GuestListComponent} from "./guest-list/guest-list.component";
import {HostListComponent} from "./host-list/host-list.component";
import {ManageBookingsComponent} from "./manage-bookings/manage-bookings.component";
import {LoginComponent} from "./admin/login.comonent";
import {AuthService} from "./admin/auth.service";
import {AdminLayoutComponent} from "./admin.layout.component";
import {AdminService} from "./admin/admin.service";
import {CryptoService} from "./shared/services/crypto.service";
import {AuthGuard} from "./shared/services/auth-guard.service";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ModalComponent} from "./core/modal/modal.component";
import {CoreModule} from "./core/core.module";
import {AWSS3Service} from "./core/aws-s3.service";
import {ImageUploadService} from "./core/image-upload.service";

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    FooterComponent,
    GuestListComponent,
    HostListComponent,
    ManageBookingsComponent,
    LoginComponent,
    AdminLayoutComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NavbarModule,
    SidebarModule,
    SpaceModule,
    AppRoutingModule,
    CoreModule
  ],
  providers: [
    ServerConfigService,
    ToasterService,
    GmapService,
    AuthService,
    AuthGuard,
    AdminService,
    CryptoService,
    AWSS3Service,
    ImageUploadService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
